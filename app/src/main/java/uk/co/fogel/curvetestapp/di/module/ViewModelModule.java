package uk.co.fogel.curvetestapp.di.module;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import uk.co.fogel.curvetestapp.di.ViewModelKey;
import uk.co.fogel.curvetestapp.viewmodel.MainViewModel;
import uk.co.fogel.curvetestapp.viewmodel.ViewModelFactory;

/**
 * Module to include ViewModel classes.
 */
@SuppressWarnings("WeakerAccess")
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindsMainViewModel(MainViewModel mainViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory viewModelFactory);

}
