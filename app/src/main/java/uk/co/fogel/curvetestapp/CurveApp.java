package uk.co.fogel.curvetestapp;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import uk.co.fogel.curvetestapp.di.DaggerAppComponent;

public class CurveApp extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().create(this);
    }

}
