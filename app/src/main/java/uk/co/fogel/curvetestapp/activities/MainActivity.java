package uk.co.fogel.curvetestapp.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;

import javax.inject.Inject;

import uk.co.fogel.curvetestapp.R;
import uk.co.fogel.curvetestapp.adapter.ContactListAdapter;
import uk.co.fogel.curvetestapp.adapter.OnContactClickListener;
import uk.co.fogel.curvetestapp.core.base.BaseActivity;
import uk.co.fogel.curvetestapp.databinding.ActivityMainBinding;
import uk.co.fogel.curvetestapp.model.enteties.Contact;
import uk.co.fogel.curvetestapp.viewmodel.MainViewModel;

import static android.Manifest.permission.READ_CONTACTS;

public class MainActivity extends BaseActivity<MainViewModel, ActivityMainBinding>
        implements OnContactClickListener {

    @Inject
    protected ContactListAdapter contactListAdapter;

    private MainViewModel viewModel;
    private ActivityMainBinding binding;

    @Override
    protected Class<MainViewModel> getViewModel() {
        return MainViewModel.class;
    }

    @Override
    protected void onCreate(Bundle instance, MainViewModel viewModel, ActivityMainBinding binding) {
        this.viewModel = viewModel;
        this.binding = binding;
        initRecyclerView();
        if (hasPhoneContactsPermission()) {
            contactListAdapter.setList(viewModel.getAllContacts());
        } else {
            requestPermission();
        }
        binding.svContacts.setActivated(true);
        binding.svContacts.setQueryHint("Search contacts");
        binding.svContacts.onActionViewExpanded();
        binding.svContacts.setIconified(false);
        binding.svContacts.clearFocus();
        binding.svContacts.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                contactListAdapter.setList(viewModel.searchContacts(newText));

                return false;
            }
        });
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        int length = grantResults.length;
        if (length > 0) {
            int grantResult = grantResults[0];
            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                contactListAdapter.setList(viewModel.getAllContacts());
            } else {
                showMessage("Permission to read contacts has been denied.");
            }
        }
    }

    private void initRecyclerView() {
        binding.rvContacts.setLayoutManager(new LinearLayoutManager(this));
        binding.rvContacts.setItemAnimator(new DefaultItemAnimator());
        binding.rvContacts.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        binding.rvContacts.setAdapter(contactListAdapter);
        contactListAdapter.setOnClickListener(this);
    }

    private boolean hasPhoneContactsPermission() {
        boolean ret = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasPermission = ContextCompat.checkSelfPermission(getApplicationContext(), READ_CONTACTS);
            if (hasPermission == PackageManager.PERMISSION_GRANTED) {
                ret = true;
            }
        } else {
            ret = true;
        }
        return ret;
    }

    // Request a runtime permission to app user.
    private void requestPermission() {
        String requestPermissionArray[] = {READ_CONTACTS};
        ActivityCompat.requestPermissions(this, requestPermissionArray, 1);
    }

    @Override
    public void onItemClick(Contact contact) {
        Intent intent = new Intent(this, ContactDetailsActivity.class);
        intent.putExtra("contact", contact);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
    }
}
