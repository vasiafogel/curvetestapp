package uk.co.fogel.curvetestapp.adapter;

import uk.co.fogel.curvetestapp.model.enteties.Contact;

public interface OnContactClickListener {

    void onItemClick(Contact contact);

}
