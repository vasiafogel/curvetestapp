package uk.co.fogel.curvetestapp.model.enteties;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Contact implements Parcelable {

    private String name;
    private String avatar;
    private ArrayList<String> phoneNumber;

    public Contact(){ }

    public Contact(String name, String avatar, ArrayList<String> phoneNumber) {
        this.name = name;
        this.avatar = avatar;
        this.phoneNumber = phoneNumber;
    }

    protected Contact(Parcel in) {
        name = in.readString();
        avatar = in.readString();
        phoneNumber = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(avatar);
        dest.writeStringList(phoneNumber);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public ArrayList<String> getPhoneNumbers() {
        return phoneNumber;
    }

    public String getPhoneNumber() {
        if (phoneNumber == null || phoneNumber.size() == 0){
            return "";
        } else {
            return phoneNumber.get(0);
        }
    }

    public void setPhoneNumber(ArrayList<String> phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
