package uk.co.fogel.curvetestapp.di;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import uk.co.fogel.curvetestapp.CurveApp;
import uk.co.fogel.curvetestapp.di.module.AppModule;

/**
 * Application Component.
 */
@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class})
public interface AppComponent extends AndroidInjector<CurveApp> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<CurveApp> {
    }
}
