package uk.co.fogel.curvetestapp.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;

import com.bumptech.glide.Glide;

import java.util.Random;

import dagger.android.support.DaggerAppCompatActivity;
import uk.co.fogel.curvetestapp.R;
import uk.co.fogel.curvetestapp.databinding.ActivityContactDetailsBinding;
import uk.co.fogel.curvetestapp.model.enteties.Contact;

public class ContactDetailsActivity extends DaggerAppCompatActivity {

    private ActivityContactDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact_details);
        initUI();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }
        return true;
    }

    private void initUI(){
        Contact contact = getIntent().getParcelableExtra("contact");
        binding.tvName.setText(contact.getName());
        binding.tvPhoneNumber.setText(contact.getPhoneNumber());
        Glide.with(this)
                .load(contact.getAvatar())
                .placeholder(R.drawable.ic_avatar_placeholder)
                .into(binding.ivAvatar);
        if (TextUtils.isEmpty(contact.getAvatar())){
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            binding.ivAvatar.setColorFilter(color);
        } else {
            binding.ivAvatar.setColorFilter(android.R.color.transparent);
        }
        binding.fabCall.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + contact.getPhoneNumber()));
            startActivity(intent);
        });
    }
}
