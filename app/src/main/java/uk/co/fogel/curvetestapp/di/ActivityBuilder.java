package uk.co.fogel.curvetestapp.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import uk.co.fogel.curvetestapp.activities.ContactDetailsActivity;
import uk.co.fogel.curvetestapp.activities.MainActivity;

/**
 * Class declare to which activities dependency are injected.
 */
@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector
    abstract ContactDetailsActivity bindContactDetailsActivity();

}
