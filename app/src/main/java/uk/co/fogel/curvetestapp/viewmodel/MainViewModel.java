package uk.co.fogel.curvetestapp.viewmodel;


import android.arch.lifecycle.ViewModel;

import java.util.ArrayList;

import javax.inject.Inject;

import uk.co.fogel.curvetestapp.model.enteties.Contact;
import uk.co.fogel.curvetestapp.model.repository.ContactRepository;

/**
 * ViewModel class for Main Activity.
 */
public class MainViewModel extends ViewModel {

    private final ContactRepository repository;

    @Inject
    MainViewModel(ContactRepository repository) {
        this.repository = repository;
    }

    public Contact getContact(String id) {
        return repository.findContact(id);
    }

    public ArrayList<Contact> getAllContacts() {
        return repository.getAllContacts();
    }

    public ArrayList<Contact> searchContacts(String text) {
        return repository.searchContacts(text);
    }


}
