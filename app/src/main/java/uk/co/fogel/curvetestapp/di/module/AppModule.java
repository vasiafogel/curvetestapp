package uk.co.fogel.curvetestapp.di.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uk.co.fogel.curvetestapp.CurveApp;

/**
 * Application Module.
 */
@Module(includes = {ViewModelModule.class})
public class AppModule {

    @Provides
    @Singleton
    Context provideContext(CurveApp application) {
        return application.getApplicationContext();
    }
}
