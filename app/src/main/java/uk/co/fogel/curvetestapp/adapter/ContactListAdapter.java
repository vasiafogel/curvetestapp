package uk.co.fogel.curvetestapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Random;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.fogel.curvetestapp.R;
import uk.co.fogel.curvetestapp.activities.ContactDetailsActivity;
import uk.co.fogel.curvetestapp.activities.MainActivity;
import uk.co.fogel.curvetestapp.model.enteties.Contact;

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Contact> contacts;
    private OnContactClickListener listener;

    @Inject
    public ContactListAdapter(Context context){
        this.context = context;
    }

    public void setList(ArrayList<Contact> contacts){
        this.contacts = contacts;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Contact contact = contacts.get(position);
        viewHolder.tvName.setText(contact.getName());
        viewHolder.tvNumber.setText(contact.getPhoneNumber());
        Glide.with(context)
                .load(contact.getAvatar())
                .placeholder(R.drawable.ic_avatar_placeholder)
                .into(viewHolder.civAvatar);
        if (TextUtils.isEmpty(contact.getAvatar())){
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            viewHolder.civAvatar.setColorFilter(color);
        } else {
            viewHolder.civAvatar.setColorFilter(android.R.color.transparent);
        }
        viewHolder.itemView.setOnClickListener(v-> listener.onItemClick(contact));
    }

    @Override
    public int getItemCount() {
        if (contacts == null){
            return 0;
        } else {
            return contacts.size();
        }
    }

    public void setOnClickListener(OnContactClickListener listener) {
        this.listener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView civAvatar;
        TextView tvName;
        TextView tvNumber;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            civAvatar = itemView.findViewById(R.id.civAvatar);
            tvName = itemView.findViewById(R.id.tvName);
            tvNumber = itemView.findViewById(R.id.tvNumber);
        }
    }

}
